import { Controller, Post, Body, Req, Res , } from '@nestjs/common';
import { Get } from '@nestjs/common/decorators';
import { ApiOperation, ApiResponse } from '@nestjs/swagger';
import { Request, Response } from 'express';
import { sendAcknowledgement } from 'utils/utils';
import { SearchDTO } from './dto/search.dto';
import { SearchService } from './search.service';
import { HttpService,} from '@nestjs/axios';
@Controller('search')
export class SearchController {
  constructor(private readonly searchService: SearchService,
    private readonly httpService: HttpService) {}

  @Get('/pageConfig')
  @ApiOperation({
    summary: 'validate the request from BAP and forward it to BPP',
  })
  @ApiResponse({
    status: 200,
    description: 'Acknowledgement of received request',
  })
 async getPageConfig(
    @Req() req: Request,
    @Res() res: Response,
  ) {
   // sendAcknowledgement(res, 'ACK');
    return this.searchService.handleSearch(res,'https://page-generator.netskill.com/api/community',);
  }

  @Get('/course')
  @ApiOperation({
    summary: 'validate the request from BAP and forward it to BPP',
  })
  @ApiResponse({
    status: 200,
    description: 'Acknowledgement of received request',
  })
  async getCourselist(
    @Req() req: Request,
    @Res() res: Response,
  ) {
   // sendAcknowledgement(res, 'ACK');
    return this.searchService.handleSearch(res,'https://page-generator.netskill.com/api/home',);
  }
  @Get("/health")
  getHealth(
    @Req() req: Request,
    @Res() res: Response,
  ) {
    return this.searchService.getHealth(res,"Working fine");
  }
}
