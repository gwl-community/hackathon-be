import { HttpService } from '@nestjs/axios';
import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { requestForwarder, sendAcknowledgement, axiosGET } from 'utils/utils';
import { SearchDTO } from './dto/search.dto';
import { Response } from 'express';
@Injectable()
export class SearchService {
  constructor(private readonly httpService: HttpService) {}

  async handleSearch(res: Response,url:string) {
    console.log('in BG');
    // TODO: Add request validation
    //  sendAcknowledgement(searchDto,httpService)
    // forward the request to BPP for discovery
    // TODO: Add registry lookup and forward to each BPP
    return axiosGET(
      url,
      this.httpService,
      res,
    );
  }
  async getHealth(res: Response,stringack: string){
    sendAcknowledgement(res,stringack)

  }
}
