import { HttpService } from '@nestjs/axios';
import { InternalServerErrorException } from '@nestjs/common';
import { Response } from 'express';
import { lastValueFrom } from 'rxjs';

const requestForwarder = async (
  url: string,
  reqData: any,
  httpService: HttpService,
) => {
  try {
    const requestOptions = {
      headers: {
        'Content-Type': 'application/json',
        random_header: 'hey there',
      },
      withCredentials: true,
      mode: 'cors',
    };
    console.log('calling request forwarder');
    return await lastValueFrom(httpService.post(url, reqData, requestOptions));
  } catch (err) {
    console.log('error in request forwarder: ', err);
    return new InternalServerErrorException(err);
  }
};

const axiosGET = async (
  url: string,
  httpService: HttpService,
  res:Response
) => {
  try {
    let a  =  await lastValueFrom(httpService.get(url));
    res.status(200).json(a.data);
  } catch (err) {
    console.log('error in request forwarder: ', err);
    return new InternalServerErrorException(err);
  }
};

const sendAcknowledgement = (res: Response, ack: string) => {
  res.status(200).json({
    message: {
      ack: {
        status: ack,
      },
    },
  });
};

export { requestForwarder, sendAcknowledgement , axiosGET};
